import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function Home() {
	return (
		<View style={styles.container}>
			<View style={styles.wrapperHeader}>
				<Text style={styles.title}>Ask Question</Text>
			</View>

			<View style={styles.wrapperUserProfile}>
				<View style={styles.userPicture} />

				<Text style={styles.userName}>Lily Allen O'Daniel</Text>
				<Text style={styles.userOccupation}>Artist</Text>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f9f9f900',
	},
	wrapperHeader: {
		paddingHorizontal: 20,
		marginBottom: 40,
	},
	wrapperUserProfile: {
		paddingHorizontal: 20,
		justifyContent: 'center',
		alignItems: 'center',
	},
	userPicture: {
		backgroundColor: 'green',
		width: 60,
		height: 60,
		borderRadius: 30,
		marginBottom: 10,
	},
	userName: {
		fontFamily: 'Inter-Bold',
		color: 'black',
		marginBottom: 5,
	},
	userOccupation: {
		fontFamily: 'Inter-Medium',
		color: 'gray',
	},
	title: {
		fontSize: 14,
		fontFamily: 'Inter-Bold',
		color: 'black',
	},
});
