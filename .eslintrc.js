module.exports = {
	root: true,
	extends: '@react-native-community',
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint'],
	overrides: [
		{
			files: ['*.ts', '*.tsx'],
			rules: {
				'@typescript-eslint/no-shadow': ['error'],
				'no-shadow': 0,
				'no-undef': 0,
				'no-dupe-args': 1,
				'no-console': 'error',
				'no-spaced-func': 0,
				'space-before-function-paren': ['error', 'never'],
				'func-style': ['error', 'declaration', { allowArrowFunctions: true }],
				'react/prop-types': 2,
				camelcase: 2,
			},
		},
	],
};
